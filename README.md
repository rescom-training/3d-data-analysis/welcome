# Welcome!

For the purposes of establishing a broad ecology of tools with which to handle your research data, we are going to be working with 3d Data under a very broad definition. Here 3d Data could take the form of 3d Scans, a 3d design created with Computer Aided Design, a 3d print or even raw number sets.
You can participate in this courses as often as you like until you have assessed your own learning as successful.

*The resources you are looking for can be [viewed in the 3D Data Analysis Gitbook](https://ejong.gitbook.io/3d-data-for-researchers/). <br>*

You can learn tools which include TinkerCAD, Rhino and Fusion360. <br>

This resource is one part of the Digital Research Skills Support Pack, provided by Research Computing Services at the University of Melbourne. <br>
For more information, [click here](https://research.unimelb.edu.au/infrastructure/research-computing-services/services/training/digital-support-pack) <br>

**To sign up for virtual training,[ click here. ](rescomunimelb.eventbrite.com.au)**